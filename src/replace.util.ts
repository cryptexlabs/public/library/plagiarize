import { DynamicStrategy, DynamicValueInterface } from './dynamic/dynamic-value.interface';
import { PlagiarizeMeYamlConfigInterface } from './plagiarize-me-yaml-config.interface';
import { DynamicUuidV4 } from './dynamic/dynamic-uuid-v4';
import { DynamicGitlabDockerRegistry } from './dynamic/dynamic-gitlab-docker-registry';
import {Logger} from './logger';

export class ReplaceUtil {

  private readonly _dynamicValues: DynamicValueInterface[];

  constructor(
    private readonly logger: Logger,
    private readonly cwd: string,
    private readonly source: Record<string, string>,
    private readonly target: Record<string, string>,
    public readonly vars: Record<string, string>,
    public readonly values: Record<string, string>,
    ) {
    this._dynamicValues = [];
  }

  public async initializeDynamicValues(sourceConfig: PlagiarizeMeYamlConfigInterface) {
    const json = JSON.stringify(sourceConfig);
    const values: DynamicValueInterface[] = [
      new DynamicGitlabDockerRegistry(this.logger, this.cwd),
      new DynamicUuidV4()
    ];

    for(const value of values){
      if(value.regex.test(json)){
        await value.initialize();
        this._dynamicValues.push(value);
      }
    }
  }

  public replaceDynamic(source: string, target: string, filePath: string){

    for(const value of this._dynamicValues){
      if(value.regex.test(source)){
        if(value.strategy === DynamicStrategy.create){
          if(value.regex.test(target) || !target){
            if(!value.value){
              if(value.regex.test(target)){
                this._logEmptyDynamicValue(value, filePath);
              }
              return target;
            }
            return value.value;
          }else{
            return target;
          }
        }
        if(value.strategy === DynamicStrategy.update){
          if(!value.value){
            if(value.regex.test(target)){
              this._logEmptyDynamicValue(value, filePath);
            }
            return target;
          }
          return value.value;
        }
      }
    }
  }

  private _logEmptyDynamicValue(value: DynamicValueInterface, filePath: string) {
    this.logger.warn(`Dynamic value could not be set in ${filePath} because ${value.failureReason}`);
  }

  public set(string: string,){

      let newString = string;
      for(const key in this.vars || {}){
          const varValue = this.vars[key];
          newString = newString.replace(`\$${key}`, varValue);
      }

      for(const key in this.values){
          const varValue = this.values[key];
          newString = newString.replace(`\$${key}`, varValue);
      }

      return newString;
  }

  public replace(string: string, replaceVarsAndValues: boolean = true) {
    let newString = string;

    if(replaceVarsAndValues){
      for(const key in this.vars || {}){
        const varValue = this.vars[key];
        newString = newString.replace(`\$${key}`, varValue);
      }

      for(const key in this.values){
        const varValue = this.values[key];
        newString = newString.replace(`\$${key}`, varValue);
      }
    }

    // tslint:disable-next-line:forin
    for (const key in this.source) {
      const sourceValue = this.source[key].toString();
      const targetValue = this.target[key].toString();

      if(typeof sourceValue !== 'string'){
        return '';
      }

      if (newString.includes(sourceValue)) {
        newString = newString.replace(new RegExp(sourceValue, 'g'), targetValue);
      }

      for (const spacingReplacement of ReplaceUtil.spacingReplacements()) {

        for (const ucWordsReplacements of ReplaceUtil.ucWordsReplacements()) {

          const transformedSourceValue =
                  sourceValue
                    .replace(spacingReplacement.regex, spacingReplacement.with)
                    .replace(ucWordsReplacements.regex, ucWordsReplacements.with);

          if(newString.includes(transformedSourceValue)) {
            const transformedTargetValue =
                    targetValue
                      .replace(spacingReplacement.regex, spacingReplacement.with)
                      .replace(ucWordsReplacements.regex, ucWordsReplacements.with);

            newString = newString.replace(transformedSourceValue, transformedTargetValue);
          }
        }

        const transformedSourceValue2 =
                sourceValue
                  .replace(spacingReplacement.regex, spacingReplacement.with)

        if (newString.includes(transformedSourceValue2.toUpperCase())) {
          const transformedTargetValue =
                  targetValue
                    .replace(spacingReplacement.regex, spacingReplacement.with)

          newString = newString.replace(new RegExp(transformedSourceValue2.toUpperCase(), 'g'), transformedTargetValue.toUpperCase());
        }

        if (newString.includes(transformedSourceValue2.toLowerCase())) {
          const transformedTargetValue =
                  targetValue
                    .replace(spacingReplacement.regex, spacingReplacement.with)

          newString = newString.replace(new RegExp(transformedSourceValue2.toLowerCase(), 'g'), transformedTargetValue.toLowerCase());
        }

      }
    }

    return newString;
  }

  public static spacingReplacements(): { regex: RegExp, with: string }[] {
    return [
      {
        regex: new RegExp('_', 'g'),
        with:  '',
      },
      {
        regex: new RegExp('-', 'g'),
        with:  '',
      },
      {
        regex: new RegExp('_', 'g'),
        with:  '-',
      },
      {
        regex: new RegExp('/', 'g'),
        with:  '-',
      },
      {
        regex: new RegExp('-', 'g'),
        with:  '_',
      },
      {
        regex: new RegExp('/', 'g'),
        with:  '_',
      },
      {
        regex: new RegExp('_', 'g'),
        with:  ' ',
      },
      {
        regex: new RegExp('/', 'g'),
        with:  ' ',
      },
      {
        regex: new RegExp('-', 'g'),
        with:  ' ',
      },
      {
        regex: new RegExp('_', 'g'),
        with:  '/',
      },
      {
        regex: new RegExp('-', 'g'),
        with:  '/',
      },
    ];
  }

  public static ucWordsReplacements(): { regex: RegExp, with: (substring: string, ...args: any[]) => string }[] {
    return [
      {
        regex: new RegExp('(?<= )[^\\s]|^.', 'g'),
        with:  a => a.toUpperCase(),
      },
      {
        regex: new RegExp('(?<=-)[^\-]|^.', 'g'),
        with:  a => a.toUpperCase(),
      },
      {
        regex: new RegExp('(?<=_)[^\_]|^.', 'g'),
        with:  a => a.toUpperCase(),
      },
    ];
  }

  public static allAlternatives(string: string): string[] {
    let alternatives = [];

    for (const spacingReplacement of this.spacingReplacements()) {
      alternatives = this.addUniques(this.caseAlternatives(string.replace(spacingReplacement.regex, spacingReplacement.with)), alternatives);
    }

    return alternatives;
  }

  private static addUniques(strings: string[], list: string[]): string[] {
    let newList = list;
    for (const string of strings) {
      newList = this.addUnique(string, newList);
    }
    return newList;
  }

  private static addUnique(string: string, list: string[]): string[] {
    const newList = list;

    if (!newList.includes(string)) {
      newList.push(string);
    }

    return newList;
  }

  public static caseAlternatives(string: string): string [] {
    let alternatives = [];

    alternatives = this.addUnique(string.toLowerCase(), alternatives);
    alternatives = this.addUnique(string.toUpperCase(), alternatives);

    for (const ucWordReplacement of this.ucWordsReplacements()) {
      alternatives = this.addUnique(string.toLowerCase().replace(ucWordReplacement.regex, ucWordReplacement.with), alternatives);
    }

    return alternatives;
  }

  public config(config: string):string {

    let newConfig = config;
    // tslint:disable-next-line:forin
    for (const key in this.source) {
      const regExpPattern = `\\$${key}(?!.*\\(\\))`;
      const regExp = new RegExp(regExpPattern, 'g');
      if(regExp.test(config)){
        newConfig = newConfig.replace(regExp, this.source[key]);
      }
    }
    return newConfig;
  }

  public replaceConfig(config: string) {
    return this.replace(this.config(config));
  }

}