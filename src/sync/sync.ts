import { CommandInterface, OptionsDefinition } from '../command.interface';
import { Init } from '../init/init';
import * as fs from 'fs';
import * as yaml from 'js-yaml';
import * as prompt from 'prompt';
import { Me } from '../me/me';
import { Main } from '../main';
import { GitRepoInterface } from '../plagiarize-me-yaml-config.interface';
import { Logger } from '../logger';
import {PlagiarizeConfigYamlInterface} from '../plagiarize-config-yaml.interface';
import {StringUtil} from '../util/string.util';

export class Sync extends Init implements CommandInterface {

  private hasNewConfigs: boolean;

  public constructor(logger: Logger) {
    super(logger);
    this.hasNewConfigs = false;
  }

  public initializeOptions(optionsDefinitions: OptionsDefinition[] = []): Record<string, any> {
    return super.initializeOptions(optionsDefinitions.concat([
      { name: 'redo', type: Boolean, default: false, description: `Overwrites existing values set in yaml path 'strings' in ${Init.CONFIG_FILE}` },
      { name: 'autoconfigure', type: Boolean, description: `Autoconfigure missing fields`, default: false },
    ]));
  }

  public setCWD(cwd: string) {
    super.setCWD(cwd);
  }

  public setSourceDirectory(sourceDirectory:string) {
    this.baseCommand.options['source-directory'] = sourceDirectory;
  }

  public setAutoconfigure(autoconfigure: boolean){
    this.baseCommand.options.autoconfigure = autoconfigure;
  }

  async run(): Promise<void> {

    await this.baseCommand.run();

    this._validatePlagiarizeExists();

    const data: PlagiarizeConfigYamlInterface =
              yaml.safeLoad(fs.readFileSync(`${this.baseCommand.cwd()}/${Init.CONFIG_FILE}`, 'utf-8')) as PlagiarizeConfigYamlInterface;

    data.strings = await this._getStringUpdatedConfig(data.repo, data.strings, 'strings');
    data.vars = (await this._getStringUpdatedConfig(data.repo, data.vars, 'vars') || undefined);

    if (!this.hasNewConfigs) {
      return;
    }

    const output = yaml.safeDump(data);

    const filePath = `${this.baseCommand.cwd()}/${Init.CONFIG_FILE}`;
    fs.writeFileSync(filePath, output, 'utf-8');

    this.logger.log(`Updated config file ${filePath}`);
  }

  private async _getStringUpdatedConfig(sourceRepo: GitRepoInterface, currentConfig: any, configType: string): Promise<any> {
    const sourceConfig = await this.getSourceConfig(sourceRepo, configType);

    const safeCurrentConfig = currentConfig ? currentConfig : {};
    const currentKeys =  Object.keys(safeCurrentConfig);


    const properties = [];
    // tslint:disable-next-line:forin
    for (const key in sourceConfig) {

      // noinspection JSUnfilteredForInLoop
      if (!currentKeys.includes(key) || this.baseCommand.options.redo) {

        const defaultValue = this.baseCommand.options.redo ? safeCurrentConfig[key] : sourceConfig[key];

        // noinspection JSUnfilteredForInLoop
        properties.push({
          name:     key,
          required: true,
          default:  defaultValue,
        });
      }
    }

    return new Promise<any>((resolve, reject) => {

      if (properties.length > 0) {
        this.hasNewConfigs = true;
        if(!this.baseCommand.options.autoconfigure){
          this.logger.log(`Please configure the following ${configType}:`);

          prompt.start();

          prompt.get(properties, (err, result) => {
            if (err) {
              reject(err);
            }

            const finalResult = safeCurrentConfig;

            // tslint:disable-next-line:forin
            for (const key in result) {
              // noinspection JSUnfilteredForInLoop
              finalResult[key] = StringUtil.getAsProperScalar(result[key]);
            }

            resolve(finalResult);
          });
        }else{
          const finalResult = {...safeCurrentConfig };
          for(const property of properties){
            finalResult[property.name] = StringUtil.getAsProperScalar(property.default);
          }
          resolve(finalResult);
        }
      } else {
        this.logger.log(`Project synchronized. No new ${configType} in source repository's ${Me.CONFIG_FILE}`);
        resolve(safeCurrentConfig);
      }
    });
  }

  private _validatePlagiarizeExists() {
    const cwd = this.baseCommand.cwd();
    if (!fs.existsSync(`${cwd}/${Init.CONFIG_FILE}`)) {
      throw new Error(`${cwd}/${Init.CONFIG_FILE} does not exist. Please use '${Main.NAME} ${Init.name.toLowerCase()}' first`);
    }
  }

  public description(): string {
    return `Ensures all keys for yaml path 'replace.strings' in source repository's ${Me.CONFIG_FILE} are ` +
      `in yaml path 'strings' in ${Init.CONFIG_FILE}`;
  }

  public name() {
    return this.constructor.name.toLowerCase();
  }

  public help() {
    super.help();
  }
}