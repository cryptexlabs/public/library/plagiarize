import { StepInterface } from '../step.interface';
import * as path from 'path';
import { Git } from 'git-interface';
import * as clone from 'git-clone';
import { Main } from '../main';
import * as crypto from 'crypto';
import * as fs from 'fs';
import * as os from 'os';
import * as mkdirp from 'mkdirp';
import { GitRepoInterface } from '../plagiarize-me-yaml-config.interface';
import {Logger} from '../logger';

export class InitCloneRepo implements StepInterface {

  private readonly cwdBaseName: string;

  constructor(private readonly cwd: string, private readonly gitRepo: GitRepoInterface, private readonly logger: Logger) {
    this.cwdBaseName = path.basename(cwd);
  }

  async run() {

    const target = this.getTarget();

    await mkdirp(path.dirname(target));

    this.logger.debug(`Cloning repo ${this.gitRepo.url} into ${target}`);

    const git = new Git({
      dir: target,
    });

    if (!fs.existsSync(target)){
      await new Promise<void>((resolve, reject) => {
        clone(this.gitRepo.url, target, { depth: '1'}, (err) => {
          if(err){
            reject(err);
          }else{
            resolve();
          }
        })
      })
    }

    await git.checkout(this.gitRepo.checkout || 'master');

    await git.pull();
  }

  public getTarget() {

    const hash = crypto.createHash('sha1').update(this.gitRepo.url).digest('hex');

    return `${os.homedir()}/.${Main.NAME}/repos/${hash}`;
  }

}