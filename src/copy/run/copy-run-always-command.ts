import {CopyRunCommandInterface} from './copy-run-command.interface';
import {Logger} from '../../logger';
import {RunAlways, RunInterface} from '../../plagiarize-me-yaml-config.interface';
import {ReplaceUtil} from '../../replace.util';
import {CopyRunCommand} from './copy-run-command';

export class CopyRunAlwaysCommand implements CopyRunCommandInterface {

    constructor(
        private readonly logger: Logger,
        private readonly cwd: string,
        private readonly replaceUtil: ReplaceUtil,
        private readonly config: RunInterface<RunAlways>,
    ) {
    }

    async run(): Promise<void> {
        const command = this.replaceUtil.replaceConfig(this.config.command);
        const onerror = this.config.onerror ? this.replaceUtil.replaceConfig(this.config.onerror) : undefined;
        await new CopyRunCommand(this.logger, this.cwd, command,  onerror).run();
    }
}