import {StepInterface} from '../../step.interface';
import {
    PlagiarizeConfigInterface,
    PlagiarizeMeYamlConfigInterface,
    RunAlways,
    RunIfMissing,
    RunInterface,
    RunWhenType
} from '../../plagiarize-me-yaml-config.interface';
import {ReplaceUtil} from '../../replace.util';
import {Logger} from '../../logger';
import {CopyRunIfFileMissingCommand} from './copy-run-if-file-missing-command';
import {CopyRunAlwaysCommand} from './copy-run-always-command';
import {CopyRunCommandInterface} from './copy-run-command.interface';

export class CopyRun implements StepInterface {

    protected readonly replaceUtil: ReplaceUtil;

    constructor(
        private readonly logger: Logger,
        private readonly cwd: string,
        private readonly config: PlagiarizeConfigInterface,
        private readonly sourceConfig: PlagiarizeMeYamlConfigInterface,
        protected readonly sourceDirectory: string
    ) {
        this.replaceUtil = new ReplaceUtil(logger, cwd, sourceConfig.replace.strings, config.strings, config.vars || {}, sourceConfig.replace.values)
    }

    public async run() {
        this.logger.log('Started running commands');

        const linearRunners: CopyRunCommandInterface[] = [];
        const parallelRunners: CopyRunCommandInterface[] = [];

        for (const config of this.sourceConfig.run || []) {
            const map    = {
                [RunWhenType.IF_MISSING]: new CopyRunIfFileMissingCommand(
                    this.logger,
                    this.cwd,
                    this.replaceUtil,
                    config as RunInterface<RunIfMissing>
                ),
                [RunWhenType.ALWAYS]:     new CopyRunAlwaysCommand(
                    this.logger,
                    this.cwd,
                    this.replaceUtil,
                    config as RunInterface<RunAlways>
                )
            }
            let type = 'always';
            if(config.when){
                type = typeof config.when === 'string' ? config.when : config.when.type
            }

            if(!map[type]) {
                throw new Error(`Invalid run when type: '${type}' `)
            }

            const runner = map[type];
            if(config.parallel){
                parallelRunners.push(runner);
            }else{
                linearRunners.push(runner);
            }
        }

        const allRunners: Promise<void>[] = parallelRunners.map(runner => runner.run()).concat([this._runLinearRunners(linearRunners)]);

        await Promise.all(allRunners);

        this.logger.log('Finished running commands');
    }

    private async _runLinearRunners(linearRunners: CopyRunCommandInterface[]): Promise<void> {
        for(const promise of linearRunners){
            await promise.run();
        }
    }

}