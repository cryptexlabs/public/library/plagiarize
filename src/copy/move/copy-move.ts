import { StepInterface } from '../../step.interface';
import { PlagiarizeConfigYamlInterface } from '../../plagiarize-config-yaml.interface';
import { MoveItemInterface, PlagiarizeMeYamlConfigInterface } from '../../plagiarize-me-yaml-config.interface';
import { move } from '../file';
import * as path from 'path';
import * as glob from 'glob';
import { ReplaceUtil } from '../../replace.util';
import { Filter } from '../filter';
import { Logger } from '../../logger';

export class CopyMove implements StepInterface {

  private readonly replaceUtil: ReplaceUtil;

  constructor(
    private readonly logger: Logger,
    private readonly cwd: string,
    private readonly config: PlagiarizeConfigYamlInterface,
    private readonly sourceConfig: PlagiarizeMeYamlConfigInterface
  ) {
    this.replaceUtil = new ReplaceUtil(logger, cwd, sourceConfig.replace.strings, config.strings, config.vars || {}, sourceConfig.replace.values)
  }

  public async run() {
    if(this.sourceConfig.move){
      this.logger.log('Moving started')

      this.logger.debug('Moving directories');
      await this._moveItems(this.sourceConfig.move.directories);
      this.logger.debug('Moving directories complete');
      this.logger.debug('Moving files');
      await this._moveItems(this.sourceConfig.move.files);
      this.logger.debug('Moving files complete');

      this.logger.log('Moving complete')
    }else{
      this.logger.log('Moving skipped. No move config')
    }
  }

  private async _moveItems(moveItems: MoveItemInterface[]) {
    const promises = [];
    for(const moveItem of moveItems) {
      promises.push(this._moveItem(moveItem));
    }
    await Promise.all(promises);
  }

  private async _moveItem(moveItem: MoveItemInterface) {
    const promises = [];
    const to = this.replaceUtil.replaceConfig(moveItem.to);

    for(const fromRaw of moveItem.names){

      const from = this.replaceUtil.replaceConfig(fromRaw);

      const fromFull = path.resolve(`${this.cwd}/${from}`)
      const toFull = path.resolve(`${this.cwd}/${to}/${path.basename(from)}`)

      if(!fromFull.includes('*')) {
        this.logger.debug(`Starting moving ${fromFull} to ${toFull}`);
        promises.push(move(fromFull, toFull, this.logger, new Filter(this.replaceUtil)).then(() => {
          this.logger.debug(`Finished moving ${fromFull} to ${toFull}`);
        }));
      }else{
        promises.push(this._moveGlob(fromFull, to));
      }

    }
    await Promise.all(promises);
  }

  private async _moveGlob(fromFull, to) {
    const files = glob.sync(fromFull);
    const promises = [];

    for(const file of files){

      const from = this.replaceUtil.replaceConfig(path.basename(file));
      const toFull = path.resolve(`${this.cwd}/${to}/${from}`)

      this.logger.debug(`Starting moving ${file} to ${toFull}`);
      promises.push(move(file, toFull, this.logger, new Filter(this.replaceUtil)).then(() => {
        this.logger.debug(`Finished moving ${file} to ${toFull}`);
      }));
    }
    await Promise.all(promises);
  }

}