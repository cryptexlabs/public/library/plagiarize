import {CommandInterface, OptionsDefinition} from '../command.interface';
import {BaseCommand} from '../base-command';
import {StepInterface} from '../step.interface';
import {CopyMove} from './move/copy-move';
import * as yaml from 'js-yaml';
import * as fs from 'fs';
import {Me} from '../me/me';
import {Init} from '../init/init';
import {CopyReplace} from './replace/copy-replace';
import {CopyCreate} from './create/copy-create';
import {CopyMerge} from './merge/copy-merge';
import {CopyTemplate} from './template/copy-template';
import {InitCloneRepo} from '../init/init-clone-repo';
import {Logger} from '../logger';
import {CopyRun} from './run/copy-run';
import {PlagiarizeMeYamlConfigInterface} from '../plagiarize-me-yaml-config.interface';
import {PlagiarizeConfigYamlInterface} from '../plagiarize-config-yaml.interface';
import {Sync} from '../sync/sync';
import * as _ from 'lodash'

export class Copy implements CommandInterface {

    private baseCommand: BaseCommand;

    public constructor(private readonly logger: Logger) {
        this.baseCommand = new BaseCommand(logger, this)
    }

    public initializeOptions(optionsDefinitions: OptionsDefinition[] = []): Record<string, any> {
        return this.baseCommand.initializeOptions(optionsDefinitions.concat([
            {name: 'source-directory', alias: 's', type: String, description: `The source directory to copy which also contains ${Me.CONFIG_FILE}`},
            {name: 'autoconfigure', type: Boolean, description: `Autoconfigure missing fields`, default: false},
            {name: 'replace-only', type: Boolean, description: `Only replace files`, default: false},
            {name: 'move-only', type: Boolean, description: `Only move files`, default: false},
            {name: 'create-only', type: Boolean, description: `Only create files`, default: false},
            {name: 'merge-only', type: Boolean, description: `Only merge files`, default: false},
            {name: 'template-only', type: Boolean, description: `Only template files`, default: false},
            {name: 'run-only', type: Boolean, description: `Only run commands`, default: false},
            // { name: 'source-config', alias: 'c', type: String, description: `The path to the ${Me.CONFIG_FILE}` }
        ]));
    }

    public setCWD(cwd: string) {
        this.baseCommand.setCWD(cwd);
    }

    public setOption(option: string, value: string) {
        this.baseCommand.options[option] = value;
    }

    public async run() {
        await this.baseCommand.run();

        const sync = new Sync(this.logger);
        sync.setCWD(this.baseCommand.cwd());
        sync.setSourceDirectory(this.baseCommand.options['source-directory']);
        sync.setAutoconfigure(this.baseCommand.options.autoconfigure);
        await sync.run();

        const configPath = `${this.baseCommand.cwd()}/${Init.CONFIG_FILE}`;
        this.logger.debug(`Started loading ${Init.CONFIG_FILE}`);
        const config: PlagiarizeConfigYamlInterface = yaml.safeLoad(fs.readFileSync(configPath, 'utf-8')) as PlagiarizeConfigYamlInterface;
        this.logger.debug(`Finished loading ${Init.CONFIG_FILE}`);

        let sourcePath = this.baseCommand.options['source-directory'];
        if (!this.baseCommand.options['source-directory']) {
            if (!config.repo?.url) {
                throw new Error(`Cannot copy from remote because configuration in ${this.baseCommand.cwd()}/${Init.CONFIG_FILE} at path 'repo.url' is empty`)
            }

            this.logger.debug(`Started checking out ${config.repo.url}`);
            const cloneStep = new InitCloneRepo(this.baseCommand.cwd(), config.repo, this.logger);
            await cloneStep.run();
            sourcePath = cloneStep.getTarget();
            this.logger.debug(`Finished checking out ${config.repo.url}`);
        }

        const sourceConfigPath = `${sourcePath}/${Me.CONFIG_FILE}`;

        this.logger.debug(`Started loading  ${Me.CONFIG_FILE}`);
        let sourceConfig: PlagiarizeMeYamlConfigInterface
                  = yaml.safeLoad(fs.readFileSync(sourceConfigPath, 'utf-8')) as PlagiarizeMeYamlConfigInterface;
        this.logger.debug(`Finished loading ${Me.CONFIG_FILE}`);

        if(config.override){
            this.logger.debug(`Applying overrides`)
            sourceConfig = _.merge(sourceConfig, config.override);
            this.logger.debug(`Final source config`);
            this.logger.debug(yaml.dump(sourceConfig));
            this.logger.debug(`Finished applying overrides`)
        }

        let steps: StepInterface[] = [];
        let only                   = false;

        if (this.baseCommand.options['replace-only']) {
            steps.push(new CopyReplace(this.logger, this.baseCommand.cwd(), config, sourceConfig, sourcePath));
        }

        if (this.baseCommand.options['create-only']) {
            steps.push(new CopyCreate(this.logger, this.baseCommand.cwd(), config, sourceConfig, sourcePath));
        }

        if (this.baseCommand.options['merge-only']) {
            steps.push(new CopyMerge(this.logger, this.baseCommand.cwd(), config, sourceConfig, sourcePath, this.baseCommand.cwd()));
        }

        if (this.baseCommand.options['template-only']) {
            steps.push(new CopyTemplate(this.logger, this.baseCommand.cwd(), config, sourceConfig, sourcePath, this.baseCommand.cwd()))
        }

        if (steps.length === 0) {
            steps = [
                new CopyReplace(this.logger, this.baseCommand.cwd(), config, sourceConfig, sourcePath),
                new CopyCreate(this.logger, this.baseCommand.cwd(), config, sourceConfig, sourcePath),
                new CopyMerge(this.logger, this.baseCommand.cwd(), config, sourceConfig, sourcePath, this.baseCommand.cwd()),
                new CopyTemplate(this.logger, this.baseCommand.cwd(), config, sourceConfig, sourcePath, this.baseCommand.cwd())
            ];
        } else {
            only = true;
        }

        if (this.baseCommand.options['run-only']) {
            only = true
        }

        if (this.baseCommand.options['move-only'] || !only) {
            await new CopyMove(this.logger, this.baseCommand.cwd(), config, sourceConfig).run();
            if (this.baseCommand.options['move-only']) {
                return;
            }
        }

        const promises = [];
        for (const step of steps) {
            promises.push(step.run())
        }

        await Promise.all(promises).catch((error) => {
            this.logger.error(error);
            this.logger.debug(error.stack);
            process.exit(1);
        });

        if (this.baseCommand.options['run-only'] || !only) {
            await new CopyRun(this.logger, this.baseCommand.cwd(), config, sourceConfig, sourcePath).run();
        }
    }

    public description(): string {
        return `Copies all configured files from source repository's ${Me.CONFIG_FILE} into working directory`;
    }

    public name() {
        return this.constructor.name.toLowerCase();
    }

    public help() {
        this.baseCommand.help();
    }
}