import { MergePatternStepInterface } from './merge-pattern-step-interface';
import { ReplaceUtil } from '../../replace.util';
import { parse } from 'dotenv';
import * as fs from 'fs';
import * as readline from 'readline';
import {Logger} from '../../logger';

export class CopyMergeDotEnv implements MergePatternStepInterface {

  constructor(
    private readonly replaceUtil: ReplaceUtil,
    private readonly logger: Logger,
    private readonly sourceDirectory: string,
    private readonly targetDirectory: string,
    private readonly file,
    private readonly tempFilePath: string
  ) {

  }

  public async create(pattern: string, createPatterns: string[]): Promise<any> {
    return this._run(pattern, false, createPatterns);
  }

  public replace(pattern: string, createPatterns: string[]): Promise<any> {
    return this._run(pattern, true, createPatterns);
  }

  public async set(pattern: string, value: string, createPatterns: string[]): Promise<any> {

    const to = this.replaceUtil.replace(this.file);
    let targetValue = this.replaceUtil.set(value);

    let dynamicValue;
    let currentTargetValue;
    const toFilePath = `${this.targetDirectory}/${to}`
    if(pattern !== '*' && fs.existsSync(toFilePath)) {
      currentTargetValue = await this._getResult(toFilePath, pattern);
    }

    dynamicValue = this.replaceUtil.replaceDynamic(value, currentTargetValue, toFilePath);

    if(targetValue === 'null'){
      targetValue = '';
    }

    return this._run(pattern, true, createPatterns, dynamicValue || targetValue);
  }

  public async skip(pattern: string, createPatterns: []): Promise<any> {
    const value = await this._getResult(this.targetDirectory, pattern);
    return await this._run(pattern, true, createPatterns, value);
  }

  public async _run(pattern: string, overwrite: boolean, createPatterns: string[], setValue?: any): Promise<void> {

    const fromFile = this.replaceUtil.config(this.file);
    const toFile = this.replaceUtil.replaceConfig(this.file);
    const sourceFile = `${this.sourceDirectory}/${fromFile}`;
    const targetFile = `${this.targetDirectory}/${toFile}`;
    if(sourceFile === targetFile){
      return;
    }

    let sourceEnv = {};
    let targetEnv = {};
    let originalEnv = {};
    sourceEnv = parse(fs.readFileSync(sourceFile));
    targetEnv = parse(fs.readFileSync(this.tempFilePath));
    if(fs.existsSync(targetFile)){
      originalEnv = parse(fs.readFileSync(targetFile));
    }

    if(!overwrite){
      const targetValue = this._getResult(this.tempFilePath, pattern);
      if(targetValue !== 'null') {
        return;
      }
    }

    let writeValue = setValue;
    if(!setValue){
      const sourceValue = this._getResult(sourceFile, pattern);
      writeValue = this.replaceUtil.replace(sourceValue.toString());
    }

    const readStream = fs.createReadStream(this.tempFilePath);

    const writeStreamPath = `${this.tempFilePath}-result`;
    const writeStream = fs.createWriteStream(writeStreamPath);

    const rl = readline.createInterface({
      input: readStream,
      output: process.stdout,
      terminal: false
    });

    const keys = [];
    let lastSourceKeyIndex = 0;
    const sourceKeys = Object.keys(sourceEnv);
    const targetKeys = Object.keys(targetEnv);

    rl.on('line',async (line) => {

      if(/.*(\/\/|\/\*|#).*plagiarize:(start|end):.*/.test(line)){
        this.logger.debug(`Omitting line '${line}' because it is a special comment line`);
        return;
      }

      if(line.includes('=')){
        const parts = line.split('=');
        const key = parts[0];

        keys.push(key)

        if(pattern !== '*'){
          const nextSourceKeyIndex = this._getObjectKeyIndex(sourceEnv, key);
          if(nextSourceKeyIndex > lastSourceKeyIndex + 1){
            const sourceKey = sourceKeys[lastSourceKeyIndex + 1];
            if(!keys.includes(sourceKey)){
              const sourceValue = sourceEnv[sourceKey];
              writeStream.write(`${sourceKey}=${sourceValue}\n`);
              keys.push(sourceKey);
            }
          }

          lastSourceKeyIndex = nextSourceKeyIndex;

          if(Object.keys(sourceEnv).includes(key)){

            if(key === pattern){
              writeStream.write(`${key}=${writeValue}\n`);
            }else{
              let targetValue;
              if(createPatterns.includes(key) && originalEnv[key] !== undefined) {
                targetValue = originalEnv[key];
              }else {
                targetValue = this._getResult(this.tempFilePath, key);
              }

              if(targetValue === 'null'){
                if(targetKeys.includes(key)) {
                  writeStream.write(`${key}=\n`);
                }else{
                  writeStream.write(`${line}\n`);
                }
              }else{
                writeStream.write(`${key}=${targetValue}\n`);
              }
            }
          }else{
            let currentTargetVal;
            if(createPatterns.includes(key) && originalEnv[key] !== undefined) {
              currentTargetVal = originalEnv[key];
            }else {
              currentTargetVal = this._getResult(targetFile, key);
            }

            if(currentTargetVal){
              writeStream.write(`${key}=${currentTargetVal}\n`);
            }else{
              writeStream.write(`${line}\n`);
            }
          }
        }else{

          if(Object.keys(sourceEnv).includes(key)){

            if(key === pattern){
              let val;
              if(createPatterns.includes(key) && originalEnv[key] !== undefined) {
                val = originalEnv[key];
              }else {
                val = sourceEnv[key];
              }

              writeStream.write(`${key}=${val}\n`);
            }else{

              let targetValue;
              if(createPatterns.includes(key) && originalEnv[key] !== undefined) {
                targetValue = originalEnv[key];
              }else {
                targetValue = this._getResult(this.tempFilePath, key);
              }

              if(targetValue === 'null'){
                if(targetKeys.includes(key)) {
                  writeStream.write(`${key}=\n`);
                }else{
                  writeStream.write(`${line}\n`);
                }
              }else{
                writeStream.write(`${key}=${targetValue}\n`);
              }
            }

          }else{
            writeStream.write(`${line}\n`);
          }
        }

      }else{
        writeStream.write(`${line}\n`);
      }
    })

    await new Promise<void>((resolve) => {
        readStream.on('close', resolve);
    })


    if(fs.existsSync(targetFile)) {

      const originalReadStream = fs.createReadStream(targetFile);
      const orl = readline.createInterface({
        input: originalReadStream,
        output: process.stdout,
        terminal: false
      });

      let newVarsReached = false
      orl.on('line',(line) => {
        if(/.*(\/\/|\/\*|#).*plagiarize:(start|end):.*/.test(line)){
          this.logger.debug(`Omitting line '${line}' because it is a special comment line`);
          return;
        }

        if(line.includes('=')){
          const parts = line.split('=');
          const key = parts[0];
          if(!keys.includes(key)){
            newVarsReached = true
            writeStream.write(`${line}\n`);
          }
        }else if(newVarsReached){
          writeStream.write(`${line}\n`);
        }
      })

      await new Promise<void>((resolve) => {
        originalReadStream.on('close', resolve);
      })
    }

    writeStream.close();

    await new Promise<void>((resolve) => {
      writeStream.on('close', resolve);
    });

    fs.renameSync(writeStreamPath, this.tempFilePath);
  }

  private _getObjectKeyIndex(object, key): number{
    let index = 0;
    for(const item of Object.keys(object)){
      if(item === key){
        return index;
      }
      index++;
    }
    return -1;
  }

  private _getResult(path: string, varName: string): string {
    if(fs.existsSync(path)){
      const envConfig = parse(fs.readFileSync(path));
      return envConfig[varName];
    }else{
      return '';
    }
  }
}
