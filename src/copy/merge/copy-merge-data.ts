import { StepInterface } from '../../step.interface';
import * as fs from 'fs';
import { clean, copy } from '../file';
import { Filter } from '../filter';
import { ReplaceUtil } from '../../replace.util';
import * as randomstring from 'randomstring';
import { DataMergeType, MergeItemInterface } from '../../plagiarize-me-yaml-config.interface';
import { CopyMergeYq } from './copy-merge-yq';
import { CopyMergeJq } from './copy-merge-jq';
import { CopyMergeDotEnv } from './copy-merge-dot-env';
import { MergePatternStepInterface } from './merge-pattern-step-interface';
import { Logger } from '../../logger';
import {StringUtil} from '../../util/string.util';
import {basename} from 'path';

export class CopyMergeData implements StepInterface {

  constructor(
    private readonly logger: Logger,
    private readonly cwd: string,
    private readonly replaceUtil: ReplaceUtil,
    private readonly sourceDirectory: string,
    private mergeItem: MergeItemInterface,
    private outputDirectory: string
  ) {
  }

  public async run(): Promise<any> {
    const file = this.replaceUtil.config(this.mergeItem.path);
    const to = this.replaceUtil.replaceConfig(file);

    this.logger.debug(`Started merging file: ${file} into file: ${to}`);

    const fileType     = this._getFileType();
    const tempFilePath = `/tmp/${randomstring.generate(8)}.${fileType}`;
    const mergeObject  = this._getMergeObject(file, tempFilePath);

    let shouldSkip = true;
    if(this.sourceDirectory !== this.outputDirectory && basename(file) === basename(to)){
      this.logger.debug(`Disable skipIfNotSameFile configs because file is the same file in a different directory`);
      shouldSkip = false;
    }

    await this._initializeTempFile(file, tempFilePath);
    await this._mergeData(mergeObject, shouldSkip);
    await this._updateTargetFile(file, tempFilePath);

    this.logger.debug(`Finished merging file: ${file} into file: ${to}`);
  }

  private async _initializeTempFile(file: string, tempFilePath: string) {
    const fromFile = this.replaceUtil.config(file);

    if (fs.existsSync(`${this.outputDirectory}/${fromFile}`)) {
      await copy(`${this.outputDirectory}/${fromFile}`, tempFilePath, this.logger, new Filter(this.replaceUtil));
    } else {
      if (this.mergeItem.type === DataMergeType.JQ) {
        const writeStream = fs.createWriteStream(tempFilePath);
        writeStream.write('{}');
        writeStream.close();
      }

      if (this.mergeItem.type === DataMergeType.YQ) {
        const outputFile = await clean(this.sourceDirectory, fromFile, this.replaceUtil);
        await copy(outputFile, tempFilePath, this.logger, new Filter(this.replaceUtil), true);
      }

      if (this.mergeItem.type === DataMergeType.DOT_ENV) {
        const outputFile = await clean(this.sourceDirectory, fromFile, this.replaceUtil);
        await copy(outputFile, tempFilePath, this.logger, new Filter(this.replaceUtil), true);
      }
    }
  }

  private _getFileType(): string {
    const fileTypeMap = {
      'jq':   'json',
      'yq':   'yaml',
      'env': 'env',
    };
    return fileTypeMap[this.mergeItem.type];
  }

  private _getMergeObject(file: string, tempFilePath: string): MergePatternStepInterface {

    const mergeTypeMap = {
      'jq':   new CopyMergeJq(this.replaceUtil, this.logger, this.sourceDirectory, this.outputDirectory, file, tempFilePath),
      'yq':   new CopyMergeYq(this.replaceUtil, this.logger, this.sourceDirectory, this.outputDirectory, file, tempFilePath),
      'env': new CopyMergeDotEnv(this.replaceUtil, this.logger, this.sourceDirectory, this.outputDirectory, file, tempFilePath),
    };

    return mergeTypeMap[this.mergeItem.type];
  }

  private async _mergeData(mergeObject: MergePatternStepInterface, shouldSkip: boolean = true) {
    for (const pattern of this.mergeItem.create || []) {
      await mergeObject.create(pattern, this.mergeItem.create || []);
    }

    for (const pattern of this.mergeItem.replace || []) {
      await mergeObject.replace(pattern, this.mergeItem.create || []);
    }

    for (const config of this.mergeItem.replaceIf || []) {
      if(StringUtil.getAsProperScalar(this.replaceUtil.replaceConfig(config.condition))){
        await mergeObject.replace(config.pattern, this.mergeItem.create || []);
      }
    }

    for (const pattern in this.mergeItem.set || {}) {
      await mergeObject.set(pattern, this.mergeItem.set[pattern], this.mergeItem.create || []);
    }

    for (const pattern of this.mergeItem.skip || []) {
      await mergeObject.skip(pattern, this.mergeItem.create || []);
    }

    if(shouldSkip){
      for (const pattern of this.mergeItem.skipIfNotSameFileName || []) {
        await mergeObject.skip(pattern, this.mergeItem.create || []);
      }
    }
  }

  private async _updateTargetFile(file: string, tempFilePath: string) {
    const to = this.replaceUtil.replaceConfig(file);
    await copy(tempFilePath, `${this.cwd}/${to}`,this.logger, new Filter(this.replaceUtil), true);
  }

}