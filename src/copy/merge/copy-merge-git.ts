import { StepInterface } from '../../step.interface';
import { Main } from '../../main';
import * as path from 'path';
import * as fs from 'fs';
import { clean, copy } from '../file';
import { Filter } from '../filter';
import * as mkdirp from 'mkdirp';
import * as randomstring from 'randomstring';
import { ReplaceUtil } from '../../replace.util';
import { exec } from 'child-process-promise';
import * as rimraf from 'rimraf';
import { Logger } from '../../logger';

export class CopyMergeGit implements StepInterface {

  constructor(
    private readonly logger: Logger,
    private readonly cwd: string,
    private readonly replaceUtil: ReplaceUtil,
    private readonly sourceDirectory: string,
    private readonly file
  ) {

  }

  public async run(): Promise<any> {
    const toFile         = this.replaceUtil.replace(this.file);
    const targetFilePath = `${this.cwd}/${toFile}`;
    const cacheTargetFilePath  = `${this.cwd}/.${Main.NAME}/cache/${toFile}.target${path.extname(path.basename(toFile))}`;
    const cacheSourceFilePath  = `${this.cwd}/.${Main.NAME}/cache/${toFile}.source${path.extname(path.basename(toFile))}`;

    if (fs.existsSync(targetFilePath) && fs.existsSync(cacheTargetFilePath) && fs.existsSync(cacheSourceFilePath)) {
      const tempFilePath = await clean(this.sourceDirectory, this.file, this.replaceUtil);

      const gitDir = `/tmp/${randomstring.generate(20)}`;

      await mkdirp(gitDir);

      const fileName = path.basename(this.file);

      await exec(`cd ${gitDir} \\
        && git init \\
        && cp ${cacheSourceFilePath} ${fileName} \\
        && git add . \\
        && git commit -m "source cache commit" || true \\
        && git checkout -b source \\
        && cp ${tempFilePath} ${fileName} \\
        && git add . \\
        && git commit -m "source commit" || true \\
        && git checkout master \\
        && git checkout -b target \\
        && cp ${cacheTargetFilePath} ${fileName} \\
        && git add . \\
        && git commit -m "target cache commit" || true \\
        && cp ${targetFilePath} ${fileName} \\
        && git add . \\
        && git commit -m "target commit" || true \\
        && git checkout master \\
        && git merge source --no-edit\\
        && git merge target --no-edit\\
        && cp ${fileName} ${targetFilePath} \\
        ;`).catch(error => {
        this.logger.error(JSON.stringify(
          error
        ))
      });

      await copy(tempFilePath, cacheSourceFilePath, this.logger, new Filter(this.replaceUtil), true);
      rimraf.sync(gitDir);
    } else {
      const outputFile = await clean(this.sourceDirectory, this.file, this.replaceUtil);
      await copy(outputFile, cacheSourceFilePath,this.logger, new Filter(this.replaceUtil));
      await copy(outputFile, targetFilePath, this.logger, new Filter(this.replaceUtil), true);
    }

    await copy(targetFilePath, cacheTargetFilePath, this.logger, new Filter(this.replaceUtil));
  }
}
