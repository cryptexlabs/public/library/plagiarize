export interface MergePatternStepInterface {
  create(pattern: string, createPatterns: string[]): Promise<any>;
  replace(pattern: string, createPatterns: string[]): Promise<any>;
  set(pattern: string, value: string, createPatterns: string[]): Promise<any>;
  skip(pattern: string, createPatterns: string[]): Promise<any>;
}