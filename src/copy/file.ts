import * as fs from 'fs';
import * as path from 'path';
import { Filter } from './filter';
import * as mkdirp from 'mkdirp';
import * as readline from 'readline';
import * as randomstring from 'randomstring';
import { ReplaceUtil } from '../replace.util';
import {Logger} from '../logger';
import {StringUtil} from '../util/string.util';

function isThere(location: string, logger: Logger): Promise<any> {
  return new Promise<any>((res, rej) => {
    fs.stat(location, (error) => {
      if(error){
        logger.debug(`file:isThere=false ${location}`);
        res(false);
      }else{
        logger.debug(`file:isThere=true ${location}`);
        res(true);
      }
    })
  })
}

async function createParent(newPath, logger: Logger) {
  const parentDir = path.dirname(newPath);
  logger.debug(`file:createParent ${parentDir}`)
  if(!await isThere(parentDir, logger)) {
    logger.debug(`file:createParent.mkdirp ${parentDir}`)
    await mkdirp(parentDir);
  }
}

export async function create(oldPath, newPath, logger: Logger, filter: Filter) {

  if(!await isThere(newPath, logger)){
    logger.debug(`file:create: copy ${oldPath} to ${newPath}`)
    await copy(oldPath, newPath, logger, filter);
  }
}

export async function copy(oldPath, newPath, logger: Logger, filter: Filter, deleteOldPath: boolean = false): Promise<any> {

  logger.debug(`file:copy ${oldPath} to ${newPath}`)
  await createParent(newPath, logger);

  if(fs.existsSync(newPath)){
    fs.unlinkSync(newPath);
  }

  return new Promise<any>(async (res, rej) => {

    const readStream = fs.createReadStream(oldPath);
    const writeStream = fs.createWriteStream(newPath);

    readStream.on('error', rej);
    writeStream.on('error', rej);

    writeStream.on('close',  () =>{
      if(deleteOldPath){
        fs.unlink(oldPath, res);
      }
      res(null);
    });

    const stream = readStream.pipe(filter).pipe(writeStream);

    stream.on('finish', () => {
      readStream.close();
      writeStream.close();
    })
  })
}

export async function move(oldPath, newPath, logger: Logger, filter: Filter) {

  function mv(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      fs.rename(oldPath, newPath,  async (err) => {
        if (err) {
          if (err.code === 'EXDEV') {
            copy(oldPath, newPath, logger, filter, true).then(resolve).catch(reject);
          } else {
            reject(err);
          }
        }else{
          resolve(null);
        }
      });
    })
  }

  if(await isThere(oldPath, logger)){
    await mv();
  }
}

export async function clean(sourceDirectory: string, file: string, replaceUtil: ReplaceUtil) {

  const outputFile = `/tmp/${randomstring.generate(8)}`;

  await new Promise<void>((resolve, reject) => {

    const writeStream = fs.createWriteStream(outputFile);
    const readStream = fs.createReadStream(`${sourceDirectory}/${file}`);

    readStream.on('error', reject);
    writeStream.on('error', reject);

    const rl = readline.createInterface({
      input : readStream,
      output: process.stdout,
      terminal: false
    });

    let skipping = false;
    rl.on('line',(line) => {
      if(!skipping) {


        let conditionalDelete = false
        let skipLine = false
        if(/.*(\/\/|\/\*|#).*plagiarize:start:conditional:delete.*/.test(line)){
          skipLine = true
          const lineParts = line.split('=');
          const rawValue = lineParts[1].trim();
          let replacedValue = replaceUtil.replaceConfig(rawValue);
          const negated = replacedValue.substr(0, 1) === '!'
          if(negated){
            replacedValue = replacedValue.substr(1);
          }
          conditionalDelete = StringUtil.getAsProperScalar(replacedValue);
          conditionalDelete = negated ? !conditionalDelete : conditionalDelete;
        }

        const alwaysDelete = /.*(\/\/|\/\*|#).*plagiarize:start:always:delete.*/.test(line);

        // Start multiline deletion
        if(alwaysDelete || conditionalDelete){
          skipping = true;
        }else{
          // Inline deletion
          if(/.*\/\*.*p:s:a:d.*\*\/.*\/\*.*p:e:a:d.*\*\/.*/.test(line)) {
            let replacedLine = line.replace(/\/\*.*p:s:a:d.*\*\/.*\/\*.*p:e:a:d.*\*\//g, '')
            replacedLine = replaceUtil.replace(replacedLine);
            writeStream.write(`${replacedLine}\n`);
          }else{
            if(!skipLine && /.*(\/\/|\/\*|#).*plagiarize:end:conditional:delete.*/.test(line) === false){
              // Don't replace vars and values strings in comment lines
              const isComment = /.*(\/\/|\/\*|#).*/.test(line)
              const replacedLine = replaceUtil.replace(line, !isComment);
              writeStream.write(`${replacedLine}\n`);
            }
          }
        }

      }else{
        // End multiline deletion
        if(/.*(\/\/|\/\*|#).*plagiarize:end:always:delete.*/.test(line)){
          skipping = false;
        }

        let conditionalDelete = false
        if(/.*(\/\/|\/\*|#).*plagiarize:end:conditional:delete.*/.test(line)){
          const lineParts = line.split('=');
          const rawValue = lineParts[1].trim();
          let replacedValue = replaceUtil.replaceConfig(rawValue);
          const negated = replacedValue.substr(0, 1) === '!'
          if(negated){
            replacedValue = replacedValue.substr(1);
          }
          conditionalDelete =  StringUtil.getAsProperScalar(replacedValue);
          conditionalDelete = negated ? !conditionalDelete : conditionalDelete;
          if(conditionalDelete){
            skipping = false
          }
        }
      }
    })
    rl.on('close', () => {
      writeStream.close();
      resolve();
    })
  })

  return outputFile;
}