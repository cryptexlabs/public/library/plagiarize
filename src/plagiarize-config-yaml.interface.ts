import { GitRepoInterface, PlagiarizeConfigInterface } from './plagiarize-me-yaml-config.interface';

export interface PlagiarizeConfigYamlInterface extends PlagiarizeConfigInterface {
  repo: GitRepoInterface;
}