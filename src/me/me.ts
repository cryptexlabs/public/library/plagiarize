import { CommandInterface, OptionsDefinition } from '../command.interface';
import * as fs from 'fs';
import { BaseCommand } from '../base-command';
import * as yaml from 'js-yaml';
import * as path from 'path';
import * as prompt from 'prompt';
import { Logger } from '../logger';

export class Me implements CommandInterface {

  public static readonly CONFIG_FILE = 'plagiarize-me.yaml'
  private readonly baseCommand: BaseCommand;

  public constructor(private readonly logger: Logger, optionsDefinitions: any[] = []) {
    this.baseCommand = new BaseCommand(logger, this);
  }

  public initializeOptions(optionsDefinitions: OptionsDefinition[] = []): Record<string, any> {
    return this.baseCommand.initializeOptions(optionsDefinitions);
  }

  public setCWD(cwd: string) {
    this.baseCommand.setCWD(cwd);
  }

  public async run() {
    await this.baseCommand.run();

    this._validatePlagiarizeMeNotExists();
    this._validateWorkingDirectoryIsGitRepo();


    const data = await this._getYaml();
    const safeData = yaml.safeDump(data);
    const output = safeData;

    const filePath = `${this.baseCommand.cwd()}/${Me.CONFIG_FILE}`;
    fs.writeFileSync(filePath, output, 'utf-8');

    this.logger.log(`Created config file ${filePath}`);
  }

  private getStringReplacements(): Promise<any> {
    this.logger.log('Please enter the name of the project. You can add additional replacements later')

    return new Promise<any>((resolve, reject) => {
      prompt.start();

      const properties = [
        {
          name: 'project',
          required: false,
          default: path.basename(this.baseCommand.cwd())
        }
      ]

      prompt.get(properties, (err, result) => {
        if (err) {
          reject(err);
        }

        resolve({
          project: result.project.trim()
        });
      });
    })
  }

  private async _getYaml(): Promise<any> {
    const replaceStrings = await this.getStringReplacements();
    return {
      replace: {
        strings: replaceStrings
      }
    };
  }

  private _validatePlagiarizeMeNotExists(){
    const cwd = this.baseCommand.cwd();
    if (fs.existsSync(`${cwd}/${Me.CONFIG_FILE}`)) {
      throw new Error(`${cwd}/${Me.CONFIG_FILE} already exists. Please update the file manually`);
    }
  }

  private _validateWorkingDirectoryIsGitRepo() {
    const cwd = this.baseCommand.cwd();
    if (!fs.existsSync(`${cwd}/.git`)) {
      throw new Error(`Working directory ${cwd} is not a git repository. (Missing '.git' folder)`);
    }
  }

  public description(): string {
    return `Creates basic ${Me.CONFIG_FILE} file`;
  }

  public name() {
    return this.constructor.name.toLowerCase();
  }

  public help() {
    this.baseCommand.help()
  }
}