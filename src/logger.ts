/* tslint:disable:no-console */
import * as chalk from 'chalk';

export class Logger {

  constructor(public debugEnabled: boolean = false) {
  }

  public log(message: string){
    console.log(message)
  }

  public warn(message: string){
    console.log(chalk.yellow(message));
  }

  public error(message: string){
    console.log(chalk.red(message));
  }

  public debug(message: string){
    if(this.debugEnabled){
      console.log(chalk.blueBright(message));
    }
  }

}