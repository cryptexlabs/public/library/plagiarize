import { GitlabGitPostPushHookCreateMergeRequest } from './gitlab-git-post-push-hook-create-merge-request';
import { GitPostPushHookInterface } from '../git-post-push-hook.interface';
import { GitRepoInterface, PushGitInterface, PushGitPostPushHook } from '../../../../plagiarize-me-yaml-config.interface';
import { Logger } from '../../../../logger';
import {ReplaceUtil} from '../../../../replace.util';

export class GitPostPushHookCreateMergeRequest implements GitPostPushHookInterface {

  constructor(
    private readonly logger: Logger,
    private readonly replaceUtil: ReplaceUtil,
    private readonly hook: PushGitPostPushHook,
    private readonly repo: GitRepoInterface,
    private readonly gitConfig: PushGitInterface
    ) {
  }

  public async run(): Promise<void> {

    const apiHandlerMap = {
      'gitlab': new GitlabGitPostPushHookCreateMergeRequest(this.logger, this.replaceUtil, this.hook, this.repo, this.gitConfig),
    }

    const handler: GitPostPushHookInterface = apiHandlerMap[this.hook.api];

    await handler.run();
  }
}