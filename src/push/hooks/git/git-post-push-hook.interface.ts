export interface GitPostPushHookInterface {
  run(): Promise<void>
}