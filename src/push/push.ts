import {CommandInterface, OptionsDefinition} from '../command.interface';
import {BaseCommand} from '../base-command';
import {Me} from '../me/me';
import {Main} from '../main';
import {Copy} from '../copy/copy';
import * as fs from 'fs';
import * as yaml from 'js-yaml';
import {
    GitRepoInterface,
    PlagiarizeMeYamlConfigInterface,
    PushGitHook,
    PushGitInterface,
    PushItemInterface,
    PushRepoItemInterface,
} from '../plagiarize-me-yaml-config.interface';
import {InitCloneRepo} from '../init/init-clone-repo';
import {Init} from '../init/init';
import {Git} from 'git-interface';
import {GitPostPushHookInterface} from './hooks/git/git-post-push-hook.interface';
import {GitPostPushHookCreateMergeRequest} from './hooks/git/create-merge-request/git-post-push-hook-create-merge-request';
import {Logger} from '../logger';
import * as os from 'os';
import {ReplaceUtil} from '../replace.util';
import * as mkdirp from 'mkdirp';
import * as path from 'path';

export class Push implements CommandInterface {

    private readonly baseCommand: BaseCommand;

    public constructor(private readonly logger: Logger, optionsDefinitions: any[] = []) {
        this.baseCommand = new BaseCommand(logger, this);
    }

    public initializeOptions(optionsDefinitions: OptionsDefinition[] = []): Record<string, any> {
        return this.baseCommand.initializeOptions(optionsDefinitions.concat([
            {name: 'commit-message', alias: 'm', type: String, description: `The commit message to use for commits created after copying this repo`},
            {name: 'source-directory', alias: 's', type: String, description: `The source directory to copy which also contains ${Me.CONFIG_FILE}`},
            {name: 'autoconfigure', type: Boolean, description: `Autoconfigure missing fields`},
        ]));
    }

    public setCWD(cwd: string) {
        this.baseCommand.setCWD(cwd);
    }

    public async run() {
        await this.baseCommand.run();

        let sourcePath = this.baseCommand.options['source-directory'];
        if (!this.baseCommand.options['source-directory']) {
            sourcePath = this.baseCommand.cwd();
        }

        const sourceConfigPath = `${sourcePath}/${Me.CONFIG_FILE}`;

        this.logger.debug(`Started loading  ${Me.CONFIG_FILE}`);
        const sourceConfig: PlagiarizeMeYamlConfigInterface
                  = yaml.safeLoad(fs.readFileSync(sourceConfigPath, 'utf-8')) as PlagiarizeMeYamlConfigInterface;
        this.logger.debug(`Finished loading ${Me.CONFIG_FILE}`);

        this.logger.log('Started pushing files');

        for (const pushConfig of sourceConfig.push) {
            const configValues = pushConfig.values || {};
            if (pushConfig.instances) {
                for (const instance of pushConfig.instances) {
                    const replaceUtil = new ReplaceUtil(
                        this.logger,
                        this.baseCommand.cwd(),
                        {},
                        {},
                        instance,
                        configValues
                    )
                    this.logger.log(`Started pushing files for instance with configuration ${JSON.stringify(instance)}`);
                    await this._push(sourceConfig.repo, pushConfig, replaceUtil)
                }
            } else {
                const replaceUtil = new ReplaceUtil(
                    this.logger,
                    this.baseCommand.cwd(),
                    {},
                    {},
                    {},
                    configValues
                )
                await this._push(sourceConfig.repo, pushConfig, replaceUtil)
            }

        }

        this.logger.log('Finished pushing files');
    }

    private async _push(sourceRepo: GitRepoInterface, pushConfig: PushItemInterface, replaceUtil: ReplaceUtil): Promise<void> {
        const promises = [];

        const gitConfig          = {
            ...pushConfig.git
        }
        gitConfig.commit.message = this.baseCommand.options['commit-message'] || replaceUtil.replaceConfig(gitConfig.commit.message);

        for (const repo of pushConfig.git.repos) {
            promises.push(this._pushGitRemote(repo, sourceRepo, gitConfig, replaceUtil));
        }

        await Promise.all(promises);
    }

    private async _pushGitRemote(
        repo: PushRepoItemInterface,
        sourceRepo: GitRepoInterface,
        gitConfig: PushGitInterface,
        replaceUtil: ReplaceUtil
    ): Promise<void> {
        const localDirectory = replaceUtil.replaceConfig(repo.local).replace('~', os.homedir());

        let targetDirectory = localDirectory;

        if (!targetDirectory || !fs.existsSync(targetDirectory)) {

            if (!repo.remote?.url) {
                this.logger.warn('Warning. Skipping repo as both remote and local are empty')
                return;
            }

            const replacedRemote: GitRepoInterface = {
                checkout: replaceUtil.replaceConfig(repo.remote.checkout),
                defaultName: replaceUtil.replaceConfig(repo.remote.defaultName),
                url: replaceUtil.replaceConfig(repo.remote.url)
            }

            this.logger.debug(`Started checking out ${replacedRemote.url}`);

            const cloneStep = new InitCloneRepo(this.baseCommand.cwd(), replacedRemote, this.logger);
            try{
                await cloneStep.run();

                targetDirectory = cloneStep.getTarget();
                this.logger.debug(`Finished checking out ${repo.remote.url}`);

                if (repo.local && repo.local.trim() && !fs.existsSync(localDirectory)) {
                    await mkdirp(path.dirname(localDirectory))
                    fs.renameSync(targetDirectory, localDirectory)
                    targetDirectory = localDirectory;
                }
            }catch (e){
                this.logger.debug(`Remote repo ${replacedRemote.url} does not exist`);

                if (repo.local && repo.local.trim() && !fs.existsSync(localDirectory)) {
                    await mkdirp(localDirectory);
                    targetDirectory = localDirectory;
                }
            }
        }

        this.logger.debug(`Started applying for ${targetDirectory}`);
        await this._apply(targetDirectory, sourceRepo);
        this.logger.debug(`Finished applying for ${targetDirectory}`);
        await this._gitPush(repo.remote, gitConfig, targetDirectory, replaceUtil);
    }

    private async _apply(targetDirectory: string, sourceRepo: GitRepoInterface) {
        const copy = new Copy(this.logger);

        copy.setCWD(targetDirectory);
        copy.setOption('source-directory', this.baseCommand.cwd());
        copy.setOption('autoconfigure', this.baseCommand.options.autoconfigure);

        if (!fs.existsSync(`${targetDirectory}/${Init.CONFIG_FILE}`)) {
            const init = new Init(this.logger);
            init.setCWD(targetDirectory);
            init.setRepo(sourceRepo);
            if (this.baseCommand.options['source-directory']) {
                init.setSourceDirectory(this.baseCommand.options['source-directory'])
            }
            await init.run();
        }

        await copy.run();
    }

    private async _gitPush(remote: GitRepoInterface, gitConfig: PushGitInterface, targetDirectory: string, replaceUtil: ReplaceUtil) {

        if (!gitConfig.commit.create) {
            this.logger.warn(`Warning: Cannot git push to ${remote.url} because 'commit' is set to false. Skipping`);
            return;
        }

        const git = new Git({
            dir: targetDirectory,
        });

        this.logger.debug('Adding files to git staging')
        await git.add()
        this.logger.debug(`Creating commit with message "${gitConfig.commit.message}"`);

        let branchName                   = gitConfig.branch;
        let shouldSkipCreateMergeRequest = false;

        const remoteBranches: string[] = (await git.getRemoteBranchList()) as string[];

        if (!remoteBranches.includes(remote.checkout)) {
            branchName                   = remote.checkout;
            shouldSkipCreateMergeRequest = true;
        }

        gitConfig.branch = branchName;

        const localBranches: string[] = (await git.getLocalBranchList()) as string[];

        if (localBranches.includes(branchName)) {
            await git.checkout(branchName).catch(e => {
                const message = typeof e === 'string' ? e : e.message;
                this.logger.warn(message.trim().replace('fatal: ', 'Warning: '))
            });
        } else {
            this.logger.debug(`Creating new branch '${branchName}'`);
            await git.createBranch(branchName).catch(e => {
                const message = typeof e === 'string' ? e : e.message;
                this.logger.warn(message.trim().replace('fatal: ', 'Warning: '))
            });
        }

        await git.commit(gitConfig.commit.message).catch(e => {
            const message = typeof e === 'string' ? e : e.message;
            this.logger.warn(message.trim().replace('error: ', 'Warning: '))
        });

        if (remote.defaultName) {
            const remotes = await git.getRemotes();
            if (!remotes.includes(remote.defaultName)) {
                const remoteDefaultName = replaceUtil.replaceConfig(remote.defaultName);
                const remoteUrl         = replaceUtil.replaceConfig(remote.url);
                this.logger.debug(`Adding remote '${remoteDefaultName}' with url '${remoteUrl}'`)
                await git.addRemote(remoteDefaultName, remoteUrl);
            }
        }

        for (const remoteName of gitConfig.remotes) {
            try{
                if(remoteBranches.includes(branchName)) {
                    await git.pull(remoteName, { branch: branchName, rebase: true });
                }
                if(gitConfig.push){
                    await git.push(remoteName);
                }
            }catch (e){
                const message = typeof e === 'string' ? e : e.message;
                this.logger.warn(message.trim().replace('error: ', 'Warning: '))
            }
        }

        if(gitConfig.push){
            await this._postPushHooks(gitConfig.hooks, remote, gitConfig, replaceUtil, shouldSkipCreateMergeRequest);
        }
    }

    private async _postPushHooks(
        hooks: Record<string, PushGitHook[]>,
        remote: GitRepoInterface,
        gitConfig: PushGitInterface,
        replaceUtil: ReplaceUtil,
        shouldSkipCreateMergeRequest: boolean) {

        if (!hooks['post-push']) {
            return;
        }

        const postPushHooks = hooks['post-push'];

        const handlerMap = {
            'create-merge-request': GitPostPushHookCreateMergeRequest
        }

        const promises = [];
        for (const hook of postPushHooks) {
            if (hook.type === 'create-merge-request' && shouldSkipCreateMergeRequest) {
                continue;
            }

            const clazz                             = handlerMap[hook.type];
            const handler: GitPostPushHookInterface = new clazz(this.logger, replaceUtil, hook, remote, gitConfig);
            promises.push(handler.run());
        }
        await Promise.all(promises);
    }

    public description(): string {
        return `Runs '${Main.NAME} ${Copy.name.toLowerCase()}' for all items in push.* in ${Me.CONFIG_FILE}`;
    }

    public name() {
        return this.constructor.name.toLowerCase();
    }

    public help() {
        this.baseCommand.help();
    }
}