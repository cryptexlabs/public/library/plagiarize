import { DynamicStrategy, DynamicValueInterface } from './dynamic-value.interface';
import { exec } from 'child-process-promise';
import {Logger} from '../logger';

export class DynamicGitlabDockerRegistry implements DynamicValueInterface {

  public readonly regex: RegExp = /\$gitlabDockerRegistry\(\)/;
  public readonly replacedValue = '$gitlabDockerRegistry()'
  public readonly strategy : DynamicStrategy = DynamicStrategy.update;
  public failureReason: string;

  private _value: string;

  constructor(private readonly logger: Logger, private readonly cwd: string) {
  }

  public get value() {
    return this._value;
  }

  public async initialize(): Promise<void> {

    const baseFailureReason = `
Gitlab docker registry has been set to ${this.replacedValue}
You must set the value manually
Your manual changes will not be overwritten`
    let origins = [];
    try {
      const execResult = await exec(`git remote get-url origin`);
      origins = execResult.stdout.trim().split('\n');

      for(const origin of origins){
        if(origin.includes('gitlab')) {
          if(origin.includes('https://')){
            this._value = this._getRegistryFromHttps(origin);
            return
          }else{
            this._value = this._getRegistryFromSSH(origin);
            return;
          }
        }
      }

      this.failureReason = `\norigin is not a gitlab repository ${baseFailureReason}`
      this._value = '';
    }catch (e){
      this.failureReason = `\neither there is no git remote named 'origin' for this git repo or the current directory is not a git repository ${baseFailureReason}`;
      this._value = ''
    }
  }

  private _getRegistryFromHttps(origin: string): string {
    // TODO
    return '';
  }

  private _getRegistryFromSSH(origin: string): string {
    const gitParts = origin.split('@');
    const urlParts = gitParts[1].split(':');
    const domain = urlParts[0];
    const projectParts = urlParts[1].split('.git');
    const projectPath = projectParts[0];
    return `registry.${domain}/${projectPath}`;
  }
}