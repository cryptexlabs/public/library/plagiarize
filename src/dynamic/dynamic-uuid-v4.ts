import { DynamicStrategy, DynamicValueInterface } from './dynamic-value.interface';
import { v4 as uuidV4 } from 'uuid';

export class DynamicUuidV4 implements DynamicValueInterface {

  public readonly regex: RegExp = /\$uuidv4\(\)/;
  public readonly replacedValue = '$uuidv4()';
  public readonly strategy: DynamicStrategy = DynamicStrategy.create;
  public failureReason: string;

  get value() {
    return uuidV4();
  }

  public async initialize(): Promise<void> {}
}