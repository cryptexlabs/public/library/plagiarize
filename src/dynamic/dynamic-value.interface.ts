export enum DynamicStrategy {
  create = 0,
  update = 1,
}

export interface DynamicValueInterface {
  regex: RegExp;
  replacedValue: string;
  failureReason: string;
  initialize(): Promise<void>
  value: string;
  strategy: DynamicStrategy
}