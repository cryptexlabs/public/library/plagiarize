FROM node:12.18.1-alpine as base

USER root
RUN mkdir /home/node/.npm-global
RUN chown node:node /home/node/.npm-global
ENV PATH=/home/node/.npm-global/bin:$PATH
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global

ARG PLAGIARIZE_VERSION=""

RUN apk add \
git \
jq \
python3 \
build-base \
make \
autoconf \
automake \
bash \
g++ \
libc6-compat \
libjpeg-turbo-dev \
libpng-dev \
nasm \
libtool \
openssh \
;

# Yq
RUN wget -q -O /usr/bin/yq $(wget -q -O - https://api.github.com/repos/mikefarah/yq/releases/latest | \
    jq -r '.assets[] | select(.name == "yq_linux_amd64") | .browser_download_url') \
    && chmod +x /usr/bin/yq

USER node

RUN npm i -g plagiarize@$PLAGIARIZE_VERSION

CMD ["plagiarize"]