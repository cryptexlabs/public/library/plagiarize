#!/usr/bin/env node

"use strict";
const plagiarize = require("plagiarize");
const main = new plagiarize.Main();
main.run();