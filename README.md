# Plagiarize (Alpha)

Good developers template projects great developers plagiarize!

Clone, search, replace and rename directories for any git project

## Why plagiarize?

Ethical plagiarism can be extremely beneficial when it's automated. 

With the advent of microservices software developers are required to maintain more projects than ever before. 
This creates great opportunities and great challenges. 

One of biggest challenges is not only keeping all the projects up to date but keeping them in a state that can be managed over time. 
As the success of projects grow with the use of microservices so does the number of microservices.
Overtime older microservices have to be either updated manually or they get left out of date. 

Some teams have sought out tools like [Coookie Cutter](https://cookiecutter.readthedocs.io/en/1.7.2/) 
The main issue with these tools is that the project template itself must be maintained! 
Also each project must still be updated manually to get the new changes from template project. If directories are moved you end up with duplicate files. 
Some of the templates that you need cause your application to be un-runnable which means it's difficult to test the result of the change. 

Plagiarize takes a new approach. We aim to enable an existing running application to be used as a template without actually creating a project template. 
The only code changes that might be required are the use of some comments in order to omit segments of code from downstream projects. 

Plagiarize offers a variety of merging strategies that can enable living projects to gain the updates from the template without causing breakages.
Breaking changes usually will motivate developers to not update their project which means that the template will only be used for new projects.

Have you ever had to manually update a `package.json` on 30+ microservices to gain an critical security update? 
Yes you can use libraries to help mitigate duplicate code but some code is not exactly duplicate. It is silightly different in some way. 

Have you ever had to add a `.env` file to 30+ microservices to enable a new environment?

Overall Plagiarize aims to eliminate or significantly reduce outdated projects and manual work required to update projects implementing the template without causing more problems for the developer.

We hope this project helps you and your team thrive in a micro-services environment rather than get be in a state of ever growing pain, stale code, and inconsistencies between projects.

## Install

#### Yq

This package depends on yq and must be installed separately see [https://github.com/mikefarah/yq](https://github.com/mikefarah/yq) for installation instructions.

#### Yarn

```bash
yarn global add plagiarize
```

##### Yarn install Troubleshooting

##### node-gyp fails to install

If yarn install fails on newer versions of nodejs due to node-gyp errors please use npm to install instead.
While using `sudo yarn global add plagiarize` would to resolve the problem, it may not be an ideal workaround for security reasons.

#### NPM
```bash
npm i -g plagiarize
```

## Running

#### Configure a project to be plagiarized

In the root folder of a git repository:

```bash
plagiarize me
```

Generates a `plagiarize-me.yaml`

#### Configure a project that will plagiarize another project

This is the project you want to be copied by other projects. Skip this step if you are only copying another project. 

In the root folder of the git repository:

```bash
plagiarize init
```

Generates a `plagiarize.yaml` file.

#### Use plagiarized project as a template for the current project

This is the new project that will copy another project. This directory contains `plagiarize.yaml` configuration.

In the root folder of th git repository:

```bash
plagiarize copy
```

Uses settings in `plagiarize.yaml` to update the current project

#### Push settings from current project to other projects

Push the settings configured in the `plagiarize-me.yaml` to projects in the `push` configuration
```bash
plagiarize push
```

## plagiarize-me.yaml

This configuration defines all the files and settings to be copied over to projects which are using the plagiarized project as a template

Example: 

```yaml
move:
  directories:
    - to: app
      names:
        - src
        - test
        - node_modules
        - features
        - coverage
        - dist
        - example
        - reports
  files:
    - to: app
      names:
        - .dockerignore
        - .prettierrc
        - nest-cli.json
        - package.json
        - tsconfig.json
        - tsconfig.spec.json
        - tsconfig.build.json
        - tslint.json
        - yarn.lock
    - to: docker
      names:
        - docker-compose.yml
        - docker-compose.override.yml
        - docker-compose.yml
        - Dockerfile
    - to: env
      names:
        - "*.env"
    - to: bin
      names:
        - "*.sh"

run:
  - command: yarn install --cwd app
    when: always
  - command: cd app && yarn build
    onerror: cd app && yarn clean
    when:
      type: if-missing
      options:
        path: app/dist
  - command: cp docker/docker-compose.override.example.yml docker/docker-compose.override.yml
    when:
      type: if-missing
      options:
        path: docker/docker-compose.override.yml
    parallel: true
  - command: cd k8s/$project && ln -s ../../env env
    when:
      type: if-missing
      options:
        path: k8s/$project/env
  - command: cd app && yarn upgrade @timerocket/nodejs-common
    when: always
  - command: cd app && yarn upgrade @timerocket/data-model
    when: always
  - command: cd app && yarn build
    when: always
  - command: |
      chmod +x ./bin/setup-gitlab-project.sh
      ./bin/setup-gitlab-project.sh
    when:
      type: if-missing
      options:
        path: .cache/gitlab-project-configured
  - command: cd k8s/$project && helm dep up
    when: always

replace:
  strings:
    description: Backend for TimeRocket user support portal
    short-description: User Support Service
    project: support-user-service
    project-short-name: support-user
    company: TimeRocket Inc.
    author: Josh Woodcock <josh.woodcock@timerocket.com>
  vars:
    rest-enabled: true
    websocket-enabled: true
    consumer-enabled: true
    elasticsearch-enabled: true
  files:
    - app/src/main.ts
    - app/src/root.ts
    - app/src/setup.ts
    - .dockerignore
    - app/.gitignore
    - app/.prettierrc
    - app/nest-cli.json
    - app/tsconfig.json
    - app/tsconfig.spec.json
    - app/tsconfig.build.json
    - app/tslint.json
    - app/README.md
    - app/src/locale/locales.ts
    - app/src/example.spec.ts
    - app/features/example.feature
    - app/features/.gitignore
    - app/features/example.feature
    - app/features/step_definitions/example.ts
    - app/src/modes/README.md
    - app/src/modes/websocket/example.html
    - app/src/modes/websocket/README.md
    - app/src/modes/shared/jwt/example-jwt-token.ts
    - app/src/modes/shared/README.md
    - env/.gitignore
    - docker/Dockerfile
    - k8s/skaffold.yaml
    - k8s/$project/.gitignore
    - k8s/$project/.helmignore
    - k8s/$project/charts/.gitkeep
    - k8s/$project/templates/env-from-configmap.yaml
    - k8s/$project/templates/_helpers.tpl
    - k8s/$project/README.md
    - k8s/README.md
    - .gitignore
    - .gitlab-ci.yml
    - bin/setup-gitlab-project.sh

create:
  files:
    - app/src/modes/rest/healthz.controller.ts
    - app/src/domains/internal/internal-domain.event.handler.factory.ts
    - app/src/domains/external/external-domain.event.handler.factory.ts
    - app/config/topics.yaml
    - app/src/modes/shared/jwt/jwt-verifier.ts
    - docker/docker-compose.yml
    - docker/docker-compose.override.example.yml
    - k8s/$project/templates/env-from-secret.yaml
  andClean:
    - app/src/config.ts
    - app/src/modes/rest/healthz.controller.ts
    - app/config/topics.yaml
    - docker/docker-compose.yml

merge:
  - type: jq
    path: app/package.json
    replace:
      - ".scripts"
      - ".husky"
      - ".envValidateConfigs"
      - ".dependencies[\"@nestjs/common\"]"
      - ".dependencies[\"@nestjs/config\"]"
      - ".dependencies[\"@nestjs/core\"]"
      - ".dependencies[\"@nestjs/platform-express\"]"
      - ".dependencies[\"@nestjs/platform-socket.io\"]"
      - ".dependencies[\"@nestjs/platform-ws\"]"
      - ".dependencies[\"@nestjs/swagger\"]"
      - ".dependencies[\"@nestjs/websockets\"]"
      - ".dependencies[\"@timerocket/nodejs-common\"]"
      - ".dependencies[\"@timerocket/data-model\"]"
      - ".dependencies[\"@types/i18n\"]"
      - ".dependencies[\"@types/node\"]"
      - ".dependencies[\"swagger-ui-express\"]"
      - ".dependencies.typescript"
      - ".dependencies[\"jwt-decode\"]"
      - ".dependencies.uuid"
      - ".dependencies.rxjs"
      - ".dependencies.rimraf"
      - ".dependencies[\"reflect-metadata\"]"
      - ".dependencies.kafkajs"
      - ".dependencies.i18n"
      - ".dependencies.express"
      - ".dependencies.dotenv"
      - ".dependencies.randomstring"
      - ".dependencies[\"class-validator\"]"
      - ".dependencies[\"class-transformer\"]"
      - ".devDependencies"
      - ".jest"
      - ".husky"
      - ".i18n"
    create:
      - ".name"
      - ".version"
      - ".description"
      - ".author"
      - ".license"
      - ".contributors"
    replaceIf:
      - condition: $elasticsearch-enabled
        pattern: ".dependencies[\"@elastic/elasticsearch\"]"

  - type: yq
    path: k8s/$project/Chart.yaml
    replace:
      - "dependencies.(name==microservice)"
      - "dependencies.(name==elasticsearch)"
  - type: yq
    path: k8s/$project/values.yaml
    replace:
      - ""
    set:
      "microservice.image.repository": $gitlabDockerRegistry()
      "microservice.deployments.rest.enabled": $rest-enabled
      "microservice.deployments.websocket.enabled": $websocket-enabled
      "microservice.deployments.consumer.enabled": $consumer-enabled
      "microservice.services.rest.enabled": $rest-enabled
      "microservice.services.websocket.enabled": $websocket-enabled
      "elasticsearch.enabled": $elasticsearch-enabled
    skip:
      - "microservice.image.tag"
      - "microservice.deployments.rest.autoscaling"
      - "microservice.deployments.websocket.autoscaling"
      - "microservice.deployments.consumer.autoscaling"
  - type: git
    path: app/src/modes/rest/rest.module.ts
  - type: git
    path: app/src/modes/consumer/consumer.module.ts
  - type: git
    path: app/src/modes/websocket/websocket.module.ts
  - type: git
    path: app/src/domains/external/external-domain.module.ts
  - type: git
    path: app/src/domains/internal/internal-domain.module.ts

templates:
  - name: environment
    replace:
      strings:
        env-name: localhost
        env-group: localhost
      values:
        kubernetes-kafka-host: infrastructure-kafka.infrastructure.svc.cluster.local
        kubernetes-graphite-host: infrastructure-graphite.infrastructure.svc.cluster.local
        graphite-port-enabled: 2003
      vars:
        kafka-host: kafka
        graphite-host:
        graphite-port:
        metrics-enabled: false

    merge:
      - type: env
        path: env/localhost.env
        set:
          "*": "*"
          CLIENT_ID: $uuidv4()
          KAFKA_HOST: $kafka-host
          METRICS_ENABLED: $metrics-enabled
          GRAPHITE_HOST: $graphite-host
          GRAPHITE_PORT: $graphite-port
    create:
      files:
        - env/localhost.secrets.env

    configs:
      - strings:
          env-name: ci
          env-group: ci
        vars:
          kafka-host: localhost
          graphite-host:
          graphite-port:
          metrics-enabled: false
      - strings:
          env-name: docker
          env-group: docker
        vars:
          kafka-host: kafka
          graphite-host:
          graphite-port:
          metrics-enabled: false
      - strings:
          env-name: e2e-test
          env-group: e2e-test
        vars:
          kafka-host: localhost
          graphite-host:
          graphite-port:
          metrics-enabled: false
      - strings:
          env-name: minikube
          env-group: minikube
        vars:
          kafka-host: $kubernetes-kafka-host
          graphite-host:
          graphite-port:
          metrics-enabled: false
      - strings:
          env-name: development.1
          env-group: development
        vars:
          kafka-host: $kubernetes-kafka-host
          graphite-host: $kubernetes-graphite-host
          graphite-port: $graphite-port-enabled
          metrics-enabled: true
      - strings:
          env-name: production.1
          env-group: production
        vars:
          kafka-host: $kubernetes-kafka-host
          graphite-host: $kubernetes-graphite-host
          graphite-port: $graphite-port-enabled
          metrics-enabled: true

  - name: kubernetes-environment
    replace:
      strings:
        env-name: development.1
        env-number: 1
        base-domain: 1.development
        env-group: development
      vars:
        rest-min-replicas: 1
        rest-max-replicas: 20
        websocket-min-replicas: 1
        websocket-max-replicas: 20
        consumer-min-replicas: 1
        consumer-max-replicas: 20
    merge:
      - type: yq
        path: k8s/$project/$env-name.values.yaml
        set:
          "microservice.deployments.rest.autoscaling.replicas.max": $rest-max-replicas
          "microservice.deployments.rest.autoscaling.replicas.min": $rest-min-replicas
          "microservice.deployments.websocket.autoscaling.replicas.max": $websocket-max-replicas
          "microservice.deployments.websocket.autoscaling.replicas.min": $websocket-min-replicas
          "microservice.deployments.consumer.autoscaling.replicas.max": $consumer-max-replicas
          "microservice.deployments.consumer.autoscaling.replicas.min": $consumer-min-replicas

    configs:
      - strings:
          env-name: production.1
          env-number: 1
          base-domain: 1.production
          env-group: production
        vars:
          rest-min-replicas: 1
          rest-max-replicas: 20
          websocket-min-replicas: 1
          websocket-max-replicas: 20
          consumer-min-replicas: 1
          consumer-max-replicas: 20

push:
  - values:
      local-base-dir: ~/Projects/TimeRocket/Assistant/Service
      remote-base-url: git@gitlab.com:timerocket/assistant/private/service
    instances:
      - project: context-service
    git:
      repos:
        - local: $local-base-dir/$project
          remote:
            url: $remote-base-url/$project.git
            checkout: master
            defaultName: origin
      commit:
        create: true
        message: "Plagiarize copy"
      branch: plagiarize
      push: true
      remotes:
        - origin
      hooks:
        post-push:
          - type: create-merge-request
            api: gitlab
            envVars:
              apiKey: SHARED_CI_ACCESS_TOKEN
            remote: origin
            targetBranch: master
            removeSourceBranch: true
```

Example project: https://gitlab.com/timerocket/assistant/public/aws-sns

## plagiarize.yaml

This contains the basic settings for the project that is plagiarizing another project

Example: 
```yaml
repo:
  url: 'git@gitlab.com:timerocket/assistant/public/aws-sns.git'
  checkout: master
strings:
  description: Determines interaction id for end user messages
  short-description: Context Service
  project: context-service
  project-short-name: context
  company: TimeRocket Inc.
  author: Josh Woodcock <josh.woodcock@timerocket.com>
vars:
  rest-enabled: false
  websocket-enabled: false
  consumer-enabled: true
```

Example project: https://gitlab.com/cryptexlabs/authf

## Synchronize a `plagiarize.yaml` with source `plagiarie-me.yaml`

If new strings or vars were added to the `plagiarize-me.yaml` you will have to synchronize your `plagiarize.yaml` with the source `plagiarize-me.yaml`

```bash
plagiarize sync
```

## Comments to exclude portions of code from downstream projects

### Always exclusions

`doSomethingThatNoOneOtherProjectShouldDo()` will always be deleted from downstream projects
```javascript
// plagiarize:start:always:delete
doSomethingThatNoOneOtherProjectShouldDo()
// plagiarize:end:always:delete
```

#### Inline always exclusion

Only the argument for the function `withAnOptionalArgument` will be excluded while `callSomething()` will NOT be excluded.

```javascript
callSomething(/* p:s:a:d */ withAnOptionalArgument /* p:e:a:d */)
```

### Conditional exclusion

Based on vars set in `plagiarize.yaml` you can conditionally remove code. This only works for vars that have boolean values. 

#### True conditional

`plagiarize.yaml`
```yaml
vars:
  skip-database-initialization: true
```

`databaseInitialize()` will be excluded if `skip-database-initialization` is true
```javascript
// plagiarize:start:conditional:delete=$skip-database-initialization
databaseInitialize()
// plagiarize:end:conditional:delete=$skip-database-initialization
```


#### Not true conditional

`plagiarize.yaml`
```yaml
vars:
  database-enabled: false
```

`databaseInitialize()` will be excluded if the `database-enabled` is false
```javascript
// plagiarize:start:conditional:delete=!$database-enabled
databaseInitialize()
// plagiarize:end:conditional:delete=!$database-enabled
```

#### Inline conditional deletion

Conditional inline deletions are not yet supported
